package com.programmer9.braingym.common;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.arch.core.util.Function;

import com.programmer9.braingym.R;

import java.util.Locale;
import java.util.logging.Logger;

public abstract class AbstractTemplateActivity extends AppCompatActivity {
    private static final Logger LOGGER = Logger.getLogger(AbstractTemplateActivity.class.getName());

    private TextToSpeech tts;
    private volatile int delayAmount = 5;
    private int lastTextViewId = -1;
    private AbstractTemplateGeneratorAsyncTask generatorAsyncTask;
    private Function<AbstractTemplateActivity, AbstractTemplateGeneratorAsyncTask> asyncTaskConstructor;
    private boolean paused;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.asyncTaskConstructor = getAsyncTaskConstructor();
        setContentView(getContentView());

        this.tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.UK);
                    tts.setSpeechRate(0.85F);
                }
            }
        });

        resetButtons();

        LOGGER.info("AddNActivity is created");
    }

    protected abstract int getContentView();

    protected abstract Function<AbstractTemplateActivity, AbstractTemplateGeneratorAsyncTask> getAsyncTaskConstructor();

    private void resetButtons() {
        getPauseResumeButton().setEnabled(false);
        getPauseResumeButton().setText(R.string.btn_pause);
        getResetButton().setEnabled(false);
        getStartButton().setEnabled(true);
        paused = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.tts != null) {
            this.tts.stop();
            this.tts.shutdown();
        }
        stopNumberGeneratorTask();
        LOGGER.info("AddNActivity is destroyed");
    }

    public void onPauseButtonClick(View view) {
        if (paused) {
            resumeGenerator();
        } else {
            pauseGenerator();
        }
    }

    public void onResetButtonClick(View view) {
        LinearLayout linearLayout = findViewById(R.id.addNHistoryLayoutId);
        linearLayout.removeAllViews();
        stopNumberGeneratorTask();
        resetButtons();
    }

    public void onStartButtonClick(View view) {
        getStartButton().setEnabled(false);
        getPauseResumeButton().setEnabled(true);
        getResetButton().setEnabled(true);
        startNumberGeneratorTask();
    }

    public TextToSpeech getTts() {
        return tts;
    }

    public int getDelayAmount() {
        return delayAmount;
    }

    public int getLastTextViewId() {
        LOGGER.info("Returning ID: " + lastTextViewId);
        return lastTextViewId;
    }

    public void setLastTextViewId(int lastTextViewId) {
        LOGGER.info("text view ID: " + lastTextViewId);
        this.lastTextViewId = lastTextViewId;
    }

    private void pauseGenerator() {
        getPauseResumeButton().setText(R.string.btn_resume);
        stopNumberGeneratorTask();
    }

    private void resumeGenerator() {
        getPauseResumeButton().setText(R.string.btn_pause);
        startNumberGeneratorTask();
    }

    private Button getPauseResumeButton() {
        return findViewById(R.id.addNPauseBtnId);
    }

    private Button getStartButton() {
        return findViewById(R.id.addNStartBtnId);
    }

    private Button getResetButton() {
        return findViewById(R.id.addNResetBtnId);
    }

    private void startNumberGeneratorTask() {
        stopNumberGeneratorTask();
        generatorAsyncTask = asyncTaskConstructor.apply(this);
        generatorAsyncTask.execute();
        paused = false;
    }

    private void stopNumberGeneratorTask() {
        if (generatorAsyncTask != null) {
            generatorAsyncTask.cancel(false);
        }
        paused = true;
    }

}