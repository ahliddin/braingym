package com.programmer9.braingym.common;

import android.os.AsyncTask;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.programmer9.braingym.R;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public abstract class AbstractTemplateGeneratorAsyncTask extends AsyncTask<Void, String, Void> {
    private static final Logger LOGGER = Logger.getLogger(AbstractTemplateGeneratorAsyncTask.class.getName());

    private WeakReference<AbstractTemplateActivity> addNActivityWeakRef;

    public AbstractTemplateGeneratorAsyncTask(AbstractTemplateActivity addNActivity) {
        this.addNActivityWeakRef = new WeakReference<>(addNActivity);
    }

    @Override
    protected Void doInBackground(Void... objects) {
        LOGGER.info("doInBackground() called, activity weak ref: " + addNActivityWeakRef.get());
        AbstractTemplateActivity addNActivity;
        while ((addNActivity = addNActivityWeakRef.get()) != null && !isCancelled()) {
            publishProgress(getTaskText());
            sleep(addNActivity.getDelayAmount());
        }
        return null;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onProgressUpdate(String... values) {
        LOGGER.info("onProgressUpdate() called, activity weak ref: " + addNActivityWeakRef.get());
        String text = values[0];
        AbstractTemplateActivity addNActivity = addNActivityWeakRef.get();
        if (addNActivity == null) {
            return;
        }
        TextView view = new TextView(addNActivity);
        view.setId(View.generateViewId());
        view.setText(text);
        view.getTextSize();
        view.setTextSize(30);

        addNActivity.getTts().speak(preProcess(text), TextToSpeech.QUEUE_ADD, null);

        LinearLayout linearLayout = addNActivity.findViewById(R.id.addNHistoryLayoutId);
        TextView prevView = linearLayout.findViewById(addNActivity.getLastTextViewId());
        if (prevView != null) {
            String additionResult = getTaskResult(prevView.getText());
            prevView.append(" --> " + additionResult);
        }

        linearLayout.addView(view, 0);
        addNActivity.setLastTextViewId(view.getId());
    }


    private void sleep(int duration) {
        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(duration));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public abstract String getTaskText();

    public abstract String getTaskResult(CharSequence numberStr);

    /**
     * To be overriden by child classes for special cases such as (replacing 'x' with 'times' for multiplication)
     */
    public String preProcess(String text) {
        return text;
    }

}
