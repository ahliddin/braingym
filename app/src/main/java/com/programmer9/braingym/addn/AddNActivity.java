package com.programmer9.braingym.addn;

import android.os.Bundle;

import androidx.arch.core.util.Function;

import com.programmer9.braingym.R;
import com.programmer9.braingym.common.AbstractTemplateActivity;
import com.programmer9.braingym.common.AbstractTemplateGeneratorAsyncTask;

public class AddNActivity extends AbstractTemplateActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_operations;
    }

    @Override
    protected Function<AbstractTemplateActivity, AbstractTemplateGeneratorAsyncTask> getAsyncTaskConstructor() {
        return new Function<AbstractTemplateActivity, AbstractTemplateGeneratorAsyncTask>() {
            @Override
            public AbstractTemplateGeneratorAsyncTask apply(AbstractTemplateActivity input) {
                return new NumberGeneratorAsyncTask(AddNActivity.this);
            }
        };
    }

}