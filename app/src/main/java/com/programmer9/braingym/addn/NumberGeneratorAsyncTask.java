package com.programmer9.braingym.addn;

import com.programmer9.braingym.common.AbstractTemplateActivity;
import com.programmer9.braingym.common.AbstractTemplateGeneratorAsyncTask;

import org.apache.commons.lang3.RandomUtils;

import static java.lang.Integer.parseInt;

public class NumberGeneratorAsyncTask extends AbstractTemplateGeneratorAsyncTask {

    NumberGeneratorAsyncTask(AbstractTemplateActivity addNActivity) {
        super(addNActivity);
    }

    @Override
    public String getTaskText() {
        return String.valueOf(RandomUtils.nextInt(1000, 9999));
    }

    @Override
    public String getTaskResult(CharSequence numberStr) {
        char[] intChar = new char[numberStr.length()];

        for (int i = 0; i < intChar.length; i++) {
            String tmpStr = String.valueOf(parseInt("" + numberStr.charAt(i)) + 1);
            intChar[i] = tmpStr.charAt(tmpStr.length() - 1); // in case it's 10 we take 0
        }

        return String.valueOf(intChar);
    }
}
