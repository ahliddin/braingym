package com.programmer9.braingym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.programmer9.braingym.addition.AdditionActivity;
import com.programmer9.braingym.addn.AddNActivity;
import com.programmer9.braingym.multiplication.MultiplicationActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onAdditionClick(View view) {
        startActivity(AdditionActivity.class);
    }

    public void onAddNClick(View view) {
        startActivity(AddNActivity.class);
    }

    public void onMultiplicationClick(View view) {
        startActivity(MultiplicationActivity.class);
    }

    private void startActivity(Class<?> clazz) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

}
