package com.programmer9.braingym.addition;

import androidx.arch.core.util.Function;

import android.os.Bundle;

import com.programmer9.braingym.R;
import com.programmer9.braingym.common.AbstractTemplateActivity;
import com.programmer9.braingym.common.AbstractTemplateGeneratorAsyncTask;

public class AdditionActivity extends AbstractTemplateActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_operations;
    }

    @Override
    protected Function<AbstractTemplateActivity, AbstractTemplateGeneratorAsyncTask> getAsyncTaskConstructor() {
        return new Function<AbstractTemplateActivity, AbstractTemplateGeneratorAsyncTask>() {
            @Override
            public AdditionGeneratorAsyncTask apply(AbstractTemplateActivity input) {
                return new AdditionGeneratorAsyncTask(AdditionActivity.this);
            }
        };
    }
}