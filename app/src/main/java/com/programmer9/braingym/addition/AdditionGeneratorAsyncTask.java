package com.programmer9.braingym.addition;

import com.programmer9.braingym.common.AbstractTemplateActivity;
import com.programmer9.braingym.common.AbstractTemplateGeneratorAsyncTask;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;

import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;

public class AdditionGeneratorAsyncTask extends AbstractTemplateGeneratorAsyncTask {

    AdditionGeneratorAsyncTask(AbstractTemplateActivity addNActivity) {
        super(addNActivity);
    }

    @Override
    public String getTaskText() {
        return RandomUtils.nextInt(10, 999) + " + " + RandomUtils.nextInt(10, 999);
    }

    @Override
    public String getTaskResult(CharSequence numberStr) {
        String[] terms = StringUtils.split(numberStr.toString(), '+');
        if (terms.length != 2) {
            throw new IllegalArgumentException("The addition expression [" + numberStr + "] is required to have 2 terms");
        }
        int firstNum = Integer.parseInt(terms[0].trim());
        int secondNum = Integer.parseInt(terms[1].trim());

        return valueOf(firstNum + secondNum);
    }
}
