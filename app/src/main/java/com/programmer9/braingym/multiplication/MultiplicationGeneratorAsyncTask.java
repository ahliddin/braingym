package com.programmer9.braingym.multiplication;

import com.programmer9.braingym.common.AbstractTemplateActivity;
import com.programmer9.braingym.common.AbstractTemplateGeneratorAsyncTask;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;

import static java.lang.String.valueOf;

public class MultiplicationGeneratorAsyncTask extends AbstractTemplateGeneratorAsyncTask {

    MultiplicationGeneratorAsyncTask(AbstractTemplateActivity addNActivity) {
        super(addNActivity);
    }

    @Override
    public String getTaskText() {
        return RandomUtils.nextInt(2, 99) + " x " + RandomUtils.nextInt(2, 9);
    }

    @Override
    public String getTaskResult(CharSequence numberStr) {
        String[] terms = StringUtils.split(numberStr.toString(), 'x');
        if (terms.length != 2) {
            throw new IllegalArgumentException("The addition expression [" + numberStr + "] is required to have 2 terms");
        }
        int firstNum = Integer.parseInt(terms[0].trim());
        int secondNum = Integer.parseInt(terms[1].trim());

        return valueOf(firstNum * secondNum);
    }

    /**
     * TTS reads 'x' as letter, we want it to read it as 'times'
     */
    @Override
    public String preProcess(String text) {
        return StringUtils.replace(text, "x", "times");
    }
}
